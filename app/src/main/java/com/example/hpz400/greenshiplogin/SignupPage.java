package com.example.hpz400.greenshiplogin;

import android.app.AlertDialog;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.facebook.share.Sharer;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.OptionalPendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;

import java.util.Arrays;
import java.util.List;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class SignupPage extends AppCompatActivity implements View.OnClickListener,GoogleApiClient.OnConnectionFailedListener{

   private EditText editName, editEmail, editPhone, editPassword, editCoPassword;
    //this is for google sign in
    private Button Submit,signingoogle;
    private SignInButton signInButton;
    private Button Logout;
    TextView name1,email1;
    LinearLayout Proof;

    //this is for facebook signin

    LoginButton loginButton;
    CallbackManager callbackManager;






    //google login api and instance
    private GoogleApiClient googleApiClient;
    private static final String TAG = MainActivity.class.getSimpleName();
    private static final int RC_SIGN_IN = 007;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup_page);

        signingoogle=(Button) findViewById(R.id.signingoogle);
        signingoogle.setOnClickListener(this);

        name1=(TextView)findViewById(R.id.Name1);
        email1=(TextView)findViewById(R.id.Email1);
        Proof=(LinearLayout)findViewById(R.id.Proof);
        Logout=(Button)findViewById(R.id.Logout);
        Logout.setOnClickListener(this);








//1.google sign in option for creating gso

        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();
//2.google apo client create
        googleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this, this)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();

//facebook fuctionality
        loginButton = (LoginButton) findViewById(R.id.login_button);
//        fblogin();

        loginButton.setReadPermissions("email");

        callbackManager = CallbackManager.Factory.create();
        // Callback registration
        loginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                // App code
                Log.d("mylog", "Successful: " + loginResult.getAccessToken());
                /*Log.d("mylog", "User ID: " + Profile.getCurrentProfile().getId());
                Log.d("mylog", "User Profile Pic Link: " + Profile.getCurrentProfile().getProfilePictureUri(500, 500));*/
            }

            @Override
            public void onCancel() {
                // App code
            }

            @Override
            public void onError(FacebookException exception) {
                // App code
                Log.d("mylog", "Successful: " + exception.toString());
            }
        });

    }

    private void fblogin() {



    }




/*        editName = (EditText) findViewById(R.id.Name);
        editPhone = (EditText) findViewById(R.id.Phone);
        editEmail = (EditText) findViewById(R.id.Email);
        editPassword = (EditText) findViewById(R.id.Password);
        editCoPassword = (EditText) findViewById(R.id.Co_Password);

        Submit = (Button) findViewById(R.id.Submit);

        //this is for sign button onclick

        Submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Signup.this, MainActivity.class);
                startActivity(i);
                Log.v("Name", editName.getText().toString());
                Log.v("Phone", editPhone.getText().toString());
                Log.v("Email", editEmail.getText().toString());
                Log.v("Password", editPassword.getText().toString());
                Log.v("Co-Password", editCoPassword.getText().toString());


                initviews(editName.getText().toString(), editPhone.getText().toString(), editEmail.getText().toString(), editPassword.getText().toString(), editCoPassword.getText().toString());

            }
      );*/



/*
    public void initviews(String name, String phone, String email, String password, String copassword) {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://refimacsales.sribarati.com/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        // userService = retrofit.create(UserService.class);
        //get the values for api

  *//*      Call<SignupPojo> call = userService.Signup(userService.getname, userService.getphone, userService.getemail, userService.getpassword, userService.getcopassword);

        call.enqueue(new Callback<SignupPojo>() {
            @Override
            public void onResponse(Call<SignupPojo> call, Response<SignupPojo> response) {
                Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                startActivity(intent);

                Toast.makeText(Signup.this, "Signup Sucessfully:", Toast.LENGTH_SHORT).show();
            }*//*

             *//*   if(response.body().getSuccess()){
                    try{

                        List<SignupDataPojo> DataPojo = response.body().getData();
                        for (int i = 0, size = DataPojo.size(); i < size; i++) {
                            String name=response.body().getData().get(0).getname();
                            String phone=response.body().getData().get(0).getphone();
                            String email=response.body().getData().get(0).getemail();
                            String password=response.body().getData().get(0).getpassword();
                            String copassword=response.body().getData().get(0).getcopassword();
                            condition if neccessary       if(person.equals("employee")){
                            }

                            Toast.makeText(LoginActivity.this, "Personlogin:"+person, Toast.LENGTH_SHORT).show();
                        }
                    }catch (Exception e){e. getMessage(); }

                }*//*


        *//*    @Override
            public void onFailure(Call<SignupPojo> call, Throwable t) {

                Log.e("", t.toString());
            }
        });*//*

    }*/

    @Override
    public void onClick(View v) {

        switch (v.getId()){
            case R.id.signingoogle:
                signin();
                break;
            case R.id.fb:
                loginButton.performClick();
//                fblogin();
                break;

            case R.id.Logout:
                signout();
                break;
    }}

    private void signout() {

        Auth.GoogleSignInApi.signOut(googleApiClient).setResultCallback(
                new ResultCallback<Status>() {
                    @Override
                    public void onResult(Status status) {
                        updataUI(false);
                    }
                });
    }

    private void signin() {

        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(googleApiClient);
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }


    //7.this will be get the data from google account and result set in to our user interface
    private void handlerResult(GoogleSignInResult result){
        if (result.isSuccess()) {

            GoogleSignInAccount acct = result.getSignInAccount();

            String personName = acct.getDisplayName();
            String Email = acct.getEmail();

            Log.e(TAG, "Name: " + personName + ", email: " + Email);

            name1.setText(personName);
            email1.setText(Email);
            Toast.makeText(this,"Success", Toast.LENGTH_SHORT).show();

            updataUI(true);
        }

        }



    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        callbackManager.onActivityResult(requestCode, resultCode, data);
        super.onActivityResult(requestCode, resultCode, data);

        // Result returned from launching the Intent from GoogleSignInApi.getSignInIntent(...);
        if (requestCode == RC_SIGN_IN) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            handlerResult(result);

        }
    }

    @Override
    public void onStart() {
        super.onStart();

        OptionalPendingResult<GoogleSignInResult> opr = Auth.GoogleSignInApi.silentSignIn(googleApiClient);
        if (opr.isDone()) {
            // If the user's cached credentials are valid, the OptionalPendingResult will be "done"
            // and the GoogleSignInResult will be available instantly.
            Log.d(TAG, "Got cached sign-in");
            GoogleSignInResult result = opr.get();
            handlerResult(result);
        } else {
            // If the user has not previously signed in on this device or the sign-in has expired,
            // this asynchronous branch will attempt to sign in the user silently.  Cross-device
            // single sign-on will occur in this branch.
            opr.setResultCallback(new ResultCallback<GoogleSignInResult>() {
                @Override
                public void onResult(GoogleSignInResult googleSignInResult) {
                    handlerResult(googleSignInResult);
                }
            });
        }
    }

    private void updataUI(boolean isLogin){

        if (isLogin==true) {
            signingoogle.setVisibility(View.GONE);
            Proof.setVisibility(View.VISIBLE);
        } else {
            signingoogle.setVisibility(View.VISIBLE);
            Proof.setVisibility(View.GONE);

        }
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

        Log.d(TAG, "onConnectionFailed:" + connectionResult);

    }




}
